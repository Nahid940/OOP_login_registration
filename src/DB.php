<?php
//include ("config.php");
namespace App;
use PDO;
class DB{
 
    private static $pdo;
    public static function myDb(){
        if(!isset(self::$pdo)){
            try{
                self::$pdo=new PDO('mysql:host=localhost;dbname=user-login',"root","");
            }
            catch(PDOException $exp){
                return $exp->getMessage();
            }
        }
        return self::$pdo;
    }
    
    
    public static function myQuery($sql){
        return self::myDb()->prepare($sql);
    }
}
?>