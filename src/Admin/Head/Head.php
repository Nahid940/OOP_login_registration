<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/21/2017
 * Time: 11:16 AM
 */
namespace App\Admin\Head;
use App\Session;
use App\user;
use PDO;


class Head extends user
{
    private $userType='head';

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }


    public function registerHeadAdmin(){
        $sql="select email from HeadAdmin where email=:email";
        $stmt=\App\DB::myQuery($sql);
        $stmt->bindValue(':email',$this->getEmail());
        $stmt->execute();
        if($stmt->rowCount()==1)
        {
            return false;
        }
        else{
        $sql="insert into HeadAdmin(name,email,mobile,password,userType,image) values(:name,:email,:mobile,:password,:userType,:image)";
        $stmt=\App\DB::myQuery($sql);
        $stmt->bindValue(':name',$this->getName());
        $stmt->bindValue(':email',$this->getEmail());
        $stmt->bindValue(':mobile',$this->getMobile());
        $stmt->bindValue(':password',$this->getPassword());
        $stmt->bindValue(':userType',$this->userType);
        $stmt->bindValue(':image',$this->getImage());
        $stmt->execute();
        return true;
        }
    }

    public function loginHeadAdmin(){
        $sql="select * from HeadAdmin where email=:email and password=:password";
        $stmt=\App\DB::myQuery($sql);
        $stmt->bindValue(':email',$this->getEmail());
        $stmt->bindValue(':password',$this->getPassword());
        $stmt->execute();
        $result=$stmt->fetch(PDO::FETCH_OBJ);
        if($result){
            Session::init();
            Session::set('login', true);
            Session::set('person_id', $result->person_id);
            Session::set('email', $result->email);
            Session::set('name', $result->name);
            header('location:AdminDashBoard.php');
        }else{
            Session::init();
            Session::set('msg',"Invalid login!");
            header('location:AdminLogReg.php');
        }
        return $result;
    }


}