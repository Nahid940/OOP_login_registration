<?php
include_once '../../vendor/autoload.php';
$head=new \App\Admin\Head\Head();

if(isset($_POST)){
    $name=$_POST['name'];
    $email=$_POST['email'];
    $mobile=$_POST['mobile'];
    $password=$_POST['password1'];

    $filename=$_FILES['image']['name'];
    $filesize=$_FILES['image']['size'];
    $filelocation=$_FILES['image']['tmp_name'];
    $div=explode('.',$filename);
    $fileExtension=strtolower(end($div));
    $uniqueName=substr(md5(time()), 0, 10).'.'.$fileExtension;
    $uploaded_image = "../../uploads/".$uniqueName;
    move_uploaded_file($filelocation,$uploaded_image);

    $head->setName($name);
    $head->setEmail($email);
    $head->setMobile($mobile);
    $head->setPassword($password);
    $head->setImage($uploaded_image);


    if($head->registerHeadAdmin()==true){
        echo 1;
    }else{
        echo "error";
    }
}
?>