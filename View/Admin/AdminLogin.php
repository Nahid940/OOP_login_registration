<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/21/2017
 * Time: 11:42 AM
 */


//\App\Session::init();
include_once '../../vendor/autoload.php';

$head=new \App\Admin\Head\Head();
if(isset($_POST['login'])){
    $email=$_POST['email'];
    $password=$_POST['password'];

    $head->setEmail($email);
    $head->setPassword($password);

    $head->loginHeadAdmin();
}
?>